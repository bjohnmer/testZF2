<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TrabajoController extends AbstractActionController
{
    public function indexAction()
    {
      return new ViewModel();
    }
    public function otroAction()
    {
      return new ViewModel();
    }
    public function recibeParametrosAction($value='')
    {
      $data = array(
                'saludo' => 'Saludo desde parámetros',
                'testArray' => array(
                                'uno' => 'Primer Valor', 
                                'dos' => 'Segundo Valor', 
                                'tres' => 'Tercer Valor', 
                                'cuatro' => 'Cuarto Valor', 
                              ),
                );
      return new ViewModel($data);
    }

    public function valoresAction()
    {
      $data = array(
                    'id' => $this->params()->fromRoute("id", null),
                    'id2' => $this->params()->fromRoute("id2", null),
                    // 'url' => $this->getRequest()->getUri()->getPath(),
                    'url' => $this->getRequest()->getUri(),
                    /*
                    |
                    | Para acceder a los valores privados del método getUri()
                    | se usan los siguientes métodos
                    | $this->getRequest()->getUri()->getPath() 
                    |           para mostrar el Path de la URL
                    | 
                    | $this->getRequest()->getUri()->getHost() 
                    |           para mostrar el nombre del dominio
                    |
                    | $this->getRequest()->getUri()->getScheme() 
                    |           para mostrar el protocolo de conexión
                    |
                    */
                  );
      return new ViewModel($data);
    }
    public function redireccionarAction()
    {
      $base = $this->getRequest()->getUri()->getScheme().'://'.$this->getRequest()->getUri()->getHost();
      // return $this->redirect()->toURL('recibeparametros'); // Redirect a un método local de la clase
      return $this->redirect()->toURL($base.'/application/test'); // Redirect a otro controlador
      /*
      |
      | redirect()->toURL() hace un redirect http simplemente
      |
      */
    }
    public function forwardAction()
    {
      return $this->forward()->dispatch('Application\Controller\Test',array('action' => 'index'));
      /*
      |
      |
      | ->forward()->dispatch() hace un llamado al método específico de una ruta
      | pero sin salirse de la ruta actual, sin perder datos
      |
      |
      */
    }
    public function renderAction()
    {
      /*
      |
      | Como se puede ver en el indexAction, de manera automática
      | reconoce el nombre de la vista que va a mostrar sin necesidad
      | de pasársela por algún parámetro u otra cosa. También se puede
      | ver que se pudiera crear una variable para crear una nueva
      | de objeto ViewModel()
      |
      |*/
      $view = new ViewModel();
      return $view;
    }

}
