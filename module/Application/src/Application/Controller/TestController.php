<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TestController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    public function testLayoutAction()
    {
      $view = new ViewModel();
      $this->layout('layout/narrow');
      $this->layout()->title = ".: Web App :.";
      return $view;
    }
    public function ajaxAction()
    {
      $view = new ViewModel();
      $view->setTerminal(true); // Inhabilita la carga del layout
      return $view;
    }
}
